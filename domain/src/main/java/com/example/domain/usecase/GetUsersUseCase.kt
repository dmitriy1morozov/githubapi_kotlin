package com.example.domain.usecase

import com.example.domain.UserRepository
import com.example.domain.model.User
import com.example.domain.model.base.ResultBase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetUsersUseCase(repo: UserRepository) :
    BaseUserUseCase<Flow<List<User>>>(repo) {

    suspend fun execute(sinceId: Int?): Flow<ResultBase<List<User>>> =
        try {
            repo.getUsers(sinceId ?: 0)
        } catch (e: RuntimeException) {
            flow {
                emit(ResultBase.Error(e.message ?: "Exception in GetUsersUseCase"))
            }
        }
}