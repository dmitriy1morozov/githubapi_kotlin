package com.example.domain.usecase

import com.example.domain.UserRepository
import com.example.domain.model.UserDetails
import com.example.domain.model.base.ResultBase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetUserDetailsUseCase(repo: UserRepository) :
    BaseUserUseCase<Flow<UserDetails>>(repo) {

    suspend fun execute(login: String?): Flow<ResultBase<UserDetails>> =
        try {
            // TODO: 24.12.21  Validate that userName is not empty
            val login1 = login ?: "Nothing"
            repo.getUserDetails(login1)
        } catch (e: RuntimeException) {
            flow {
                emit(ResultBase.Error(e.message ?: "Exception in GetUserDetailsUseCase"))
            }
        }
}