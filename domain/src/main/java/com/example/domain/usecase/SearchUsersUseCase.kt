package com.example.domain.usecase

import com.example.domain.UserRepository
import com.example.domain.model.User
import com.example.domain.model.base.ResultBase

class SearchUsersUseCase(repo: UserRepository) :
    BaseUserUseCase<List<User>>(repo) {

    suspend fun execute(criteria: String?, page: Int?, perPage: Int?): ResultBase<List<User>> =
        try {
            repo.searchUsers(criteria, page, perPage)
        } catch (e: RuntimeException) {
            ResultBase.Error(e.message ?: "Exception in SearchUsersUseCase")
        }
}