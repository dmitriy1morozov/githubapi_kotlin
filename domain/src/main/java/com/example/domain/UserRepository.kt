package com.example.domain

import com.example.domain.model.User
import com.example.domain.model.UserDetails
import com.example.domain.model.base.ResultBase
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    fun getUserDetails(login: String): Flow<ResultBase<UserDetails>>

    fun getUsers(sinceId: Int): Flow<ResultBase<List<User>>>

    suspend fun searchUsers(
        criteria: String?,
        page: Int?,
        perPage: Int?
    ): ResultBase<List<User>>
}