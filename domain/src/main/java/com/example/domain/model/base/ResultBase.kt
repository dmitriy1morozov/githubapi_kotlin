package com.example.domain.model.base

sealed class ResultBase<out Success : Any> {
    /**
     * Success response with body
     */
    data class Success<Success : Any>(val body: Success) : ResultBase<Success>()

    /**
     * Failure response with errorBody: String and optional code:Int?
     */
    data class Error(val errorBody: String, val code: Int? = -1) :
        ResultBase<Nothing>()

    data class Loading(val status: Boolean) : ResultBase<Nothing>()
}