package com.example.domain.model

data class User(
    val avatar_url: String?,
    val html_url: String?,
    val id: Int,
    val login: String?,
)