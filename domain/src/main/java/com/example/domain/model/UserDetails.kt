package com.example.domain.model

data class UserDetails(
        val login: String,
        val followers: Int?,
        val id: Int?,
        val name: String?,
        val public_gists: Int?,
        val public_repos: Int?
)