package com.example.githubapi_kotlin.di.presentation

import com.example.githubapi_kotlin.presentation.userdetails.UserDetailsFragment
import com.example.githubapi_kotlin.presentation.userlist.UserListFragment
import dagger.Subcomponent

@PresentationScope
@Subcomponent(modules = [ViewModelModule::class, UseCasesModule::class])
interface PresentationComponent {

    fun inject(fragment: UserDetailsFragment)
    fun inject(viewModel: UserListFragment)
}