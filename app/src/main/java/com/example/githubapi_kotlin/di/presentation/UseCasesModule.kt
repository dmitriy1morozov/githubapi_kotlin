package com.example.githubapi_kotlin.di.presentation

import com.example.domain.UserRepository
import com.example.domain.usecase.GetUserDetailsUseCase
import com.example.domain.usecase.GetUsersUseCase
import com.example.domain.usecase.SearchUsersUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCasesModule {

    @Provides
    fun provideGetUserDetailsUseCase(repo: UserRepository): GetUserDetailsUseCase =
        GetUserDetailsUseCase(repo)

    @Provides
    fun provideGetUsersUseCase(repo: UserRepository): GetUsersUseCase =
        GetUsersUseCase(repo)

    @Provides
    fun provideSearchUsersUseCase(repo: UserRepository): SearchUsersUseCase =
        SearchUsersUseCase(repo)
}