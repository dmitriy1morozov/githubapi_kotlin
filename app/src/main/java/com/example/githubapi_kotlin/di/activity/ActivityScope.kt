package com.example.githubapi_kotlin.di.activity

import javax.inject.Scope

@Scope
annotation class ActivityScope