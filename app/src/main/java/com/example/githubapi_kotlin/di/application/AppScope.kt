package com.example.githubapi_kotlin.di.application

import javax.inject.Scope

@Scope
annotation class AppScope
