package com.example.githubapi_kotlin.di.activity

import androidx.appcompat.app.AppCompatActivity
import com.example.githubapi_kotlin.di.presentation.PresentationComponent
import dagger.BindsInstance
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [])
interface BaseActivityComponent {

    fun newPresentationComponent(): PresentationComponent

    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance activity: AppCompatActivity): BaseActivityComponent
    }
}