package com.example.githubapi_kotlin.di.application

import com.example.data.di.DataComponent
import com.example.githubapi_kotlin.di.activity.BaseActivityComponent
import dagger.Component

@AppScope
@Component(modules = [], dependencies = [DataComponent::class])
interface AppComponent {

    fun newBaseActivityComponentFactory(): BaseActivityComponent.Factory

}