package com.example.githubapi_kotlin.di.presentation

import androidx.lifecycle.ViewModel
import com.example.githubapi_kotlin.presentation.userdetails.UserDetailsViewModel
import com.example.githubapi_kotlin.presentation.userlist.UserListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserDetailsViewModel::class)
    abstract fun userDetailsViewModel(viewModel: UserDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun userListViewModel(viewModel: UserListViewModel): ViewModel
}