package com.example.githubapi_kotlin.di.presentation

import javax.inject.Scope

@Scope
annotation class PresentationScope
