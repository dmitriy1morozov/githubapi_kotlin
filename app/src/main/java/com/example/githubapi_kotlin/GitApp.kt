package com.example.githubapi_kotlin

import android.app.Application
import com.example.data.di.DaggerDataComponent
import com.example.data.di.DataComponent
import com.example.data.di.appcontext.AppContextModule
import com.example.githubapi_kotlin.di.application.AppComponent
import com.example.githubapi_kotlin.di.application.DaggerAppComponent

class GitApp : Application() {

    private val dataLayerComponent: DataComponent = DaggerDataComponent.factory()
        .create(AppContextModule(this))
    val appLayerComponent: AppComponent =
        DaggerAppComponent.builder()
            .dataComponent(dataLayerComponent)
            .build()

    override fun onCreate() {
        super.onCreate()
        dataLayerComponent.getGitApp().onCreate()
    }
}