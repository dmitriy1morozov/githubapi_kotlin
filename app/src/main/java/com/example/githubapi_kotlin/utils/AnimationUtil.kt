package com.example.githubapi_kotlin.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.View
import android.view.View.ALPHA
import android.view.animation.DecelerateInterpolator


object AnimationUtil {

    fun getFadeIn(view: View): Animator {
        val objectAnimator = ObjectAnimator.ofFloat(view, ALPHA, 0f, 1f)
        objectAnimator.duration = 300
        objectAnimator.interpolator = DecelerateInterpolator()
        return objectAnimator
    }

    fun getFadeOut(view: View): Animator {
        val objectAnimator = ObjectAnimator.ofFloat(view, ALPHA, 1f, 0f)
        objectAnimator.duration = 300
        objectAnimator.interpolator = DecelerateInterpolator()
        return objectAnimator
    }
}