package com.example.githubapi_kotlin.utils

import android.graphics.Outline
import android.view.View
import android.view.ViewOutlineProvider
import android.view.ViewTreeObserver
import androidx.viewbinding.ViewBinding

inline fun <T: Any> ifNonNull(vararg elements: T?, closure: () -> Unit) {
    if (elements.all { it != null }) {
        closure()
    }
}

inline fun <T : ViewBinding> T.afterMeasured(layout: T, crossinline f: T.() -> Unit) {
    layout.root.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (layout.root.measuredWidth > 0 && layout.root.measuredHeight > 0) {
                layout.root.viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}

fun clipCircle(view: View) {
    val viewOutlineProvider: ViewOutlineProvider = object : ViewOutlineProvider() {
        override fun getOutline(view: View, outline: Outline) {
            outline.setRoundRect(0, 0, view.width, view.height, 5000f)
        }
    }
    view.outlineProvider = viewOutlineProvider
    view.clipToOutline = true
}