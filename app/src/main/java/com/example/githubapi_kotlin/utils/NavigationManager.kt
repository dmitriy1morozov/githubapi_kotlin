package com.example.githubapi_kotlin.utils

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavOptions.Builder
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigator.Extras
import androidx.viewbinding.ViewBinding
import com.example.githubapi_kotlin.utils.KeyboardUtil.hideKeyboard

class NavigationManager(
    private var layout: ViewBinding? = null,
    private var activity: AppCompatActivity? = null,
    private var navHostId: Int = 0
) {
    fun back() {
        layout?.let {
            hideKeyboard(layout!!.root)
            Navigation.findNavController(layout!!.root).popBackStack()
        }
                ?: activity?.let {
                    hideKeyboard(activity)
                    activity!!.onBackPressed()
                }
    }

    fun to(actionId: Int) {
        processNavigation(null, null, actionId, null, null)
    }

    fun to(actionId: Int, args: Bundle?) {
        processNavigation(null, null, actionId, args, null)
    }

    fun to(actionId: Int, args: Bundle?, extras: Extras?) {
        processNavigation(null, null, actionId, args, extras)
    }

    fun to(popTo: Int, inclusive: Boolean, actionId: Int, args: Bundle?) {
        processNavigation(popTo, inclusive, actionId, args, null)
    }

    //==============================================================================================
    private fun processNavigation(popTo: Int?, inclusive: Boolean?, actionId: Int, args: Bundle?, extras: Extras?) {
        if (!canNavigate(actionId)) {
            Log.e(javaClass.canonicalName, "Error: cannot navigate to the requested action")
            return
        }

        hideKeyboard()
        val navOptionsBuilder = Builder()

        ifNonNull(popTo, inclusive) {
            navOptionsBuilder.setPopUpTo(popTo!!, inclusive!!)
        }

        try {
            navController.navigate(actionId, args, navOptionsBuilder.build(), extras)
        } catch (ise: IllegalStateException) {
            Log.e(javaClass.canonicalName, "Error: " + ise.message)
        }
    }

    private fun canNavigate(actionId: Int): Boolean {
        return layout?.run {
            val currentDestination = Navigation.findNavController(layout!!.root).currentDestination
            currentDestination?.getAction(actionId) != null
        } ?: true
    }

    private fun hideKeyboard() {
        layout?.let { hideKeyboard(layout!!.root) }
                ?: activity?.let { hideKeyboard(activity) }
    }

    @get:Throws(IllegalStateException::class)
    private val navController: NavController
        get() =
            if (layout != null) {
                Navigation.findNavController(layout!!.root)
            } else if (activity != null && navHostId != 0) {
                Navigation.findNavController(activity!!, navHostId)
            } else {
                throw IllegalStateException("Couldn't find navController. Either no View provided or activity not initialized correctly")
            }
}

