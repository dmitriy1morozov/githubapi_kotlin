package com.example.githubapi_kotlin.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.RecyclerView.State
import com.example.githubapi_kotlin.utils.DisplayUtil.dpToPx

class SpaceItemDecoration(private val leftDp: Int, private val rightDp: Int, private val verticalDp: Int) : ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
        val context = view.context
        outRect.left = dpToPx(context, leftDp)
        outRect.right = dpToPx(context, rightDp)
        outRect.bottom = dpToPx(context, verticalDp)
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = dpToPx(context, verticalDp)
        }
    }
}