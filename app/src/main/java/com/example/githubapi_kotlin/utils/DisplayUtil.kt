package com.example.githubapi_kotlin.utils

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue


object DisplayUtil {

    fun dpToPx(context: Context, dp: Int): Int {
        val displayMetrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), displayMetrics).toInt()
    }

    fun getDisplayHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    fun getDisplayWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }
}