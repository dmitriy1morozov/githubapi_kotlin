package com.example.githubapi_kotlin.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    protected val loadingTimeout = 1000L

    protected val _isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
    protected val _errorLiveData: MutableLiveData<Event<String?>> = MutableLiveData()
    protected val _networkStateLiveData: MutableLiveData<Boolean> = MutableLiveData()

    val isLoadingLiveData: LiveData<Boolean> = _isLoadingLiveData
    val errorLiveData: LiveData<Event<String?>> = _errorLiveData
    val networkStateLiveData: LiveData<Boolean> = _networkStateLiveData

    fun onInternetStateChanged(isConnected: Boolean) {
        // Here we may proceed with work related to dropped/enabled connection.
        _networkStateLiveData.postValue(isConnected)
    }

    protected fun isLoading(status: Boolean) {
        _isLoadingLiveData.postValue(status)
    }
}