package com.example.githubapi_kotlin.presentation.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseFragmentMvvm<out Binding : ViewBinding, out ViewModel : BaseViewModel>(
    inflate: Inflate<Binding>,
    private val clazz: Class<ViewModel>
) :
    BaseFragment<Binding, ViewModel>(inflate) {


    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    protected val viewModel: ViewModel
        get() =
            ViewModelProvider(this, viewModelFactory).get(clazz)

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            viewModel.onInternetStateChanged(true)
        }

        override fun onLosing(network: Network, maxMsToLive: Int) {
            super.onLosing(network, maxMsToLive)
            viewModel.onInternetStateChanged(false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModelState()
    }

    override fun onStart() {
        super.onStart()
        registerConnectivityState()
        disposable = CompositeDisposable()
    }

    override fun onStop() {
        super.onStop()
        unregisterConnectivityState()
        disposable.dispose()
    }

    //==============================================================================================
    protected open fun observeViewModelState() {
        viewModel.isLoadingLiveData
            .observe(viewLifecycleOwner) { isLoading: Boolean ->
                handleIsLoading(isLoading)
            }
        viewModel.errorLiveData
            .observe(viewLifecycleOwner) { errorEvent: Event<String?> ->
                errorEvent.popContent()?.let { message: String ->
                    handleError(message)
                }
            }
        viewModel.networkStateLiveData
            .observe(viewLifecycleOwner) { isConnected: Boolean ->
                handleConnectionStateChanges(isConnected)
            }
    }

    protected fun <T, LD : LiveData<T>> observe(liveData: LD, onChanged: (T) -> Unit) {
        liveData.observe(viewLifecycleOwner, { value -> value?.let(onChanged) })
    }


    private fun registerConnectivityState() {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.registerNetworkCallback(
            NetworkRequest.Builder().build(),
            networkCallback
        )
    }

    private fun unregisterConnectivityState() {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }
}