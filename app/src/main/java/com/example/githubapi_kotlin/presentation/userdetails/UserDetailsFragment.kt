package com.example.githubapi_kotlin.presentation.userdetails

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.transition.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.example.domain.model.UserDetails
import com.example.githubapi_kotlin.R
import com.example.githubapi_kotlin.databinding.FragmentUserDetailsBinding
import com.example.githubapi_kotlin.presentation.common.BaseFragmentMvvm
import com.example.githubapi_kotlin.utils.clipCircle

class UserDetailsFragment :
    BaseFragmentMvvm<FragmentUserDetailsBinding, UserDetailsViewModel>(
        FragmentUserDetailsBinding::inflate,
        UserDetailsViewModel::class.java
    ) {

    private lateinit var userParam: UserParam

    //============= Lifecycle ======================================================================
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presentationComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pullArgs()

        initTransitions()
        initDetailsUi()
        observeViewModelState()
        initListeners()

        viewModel.fetchUserDetails(userParam.login)
    }

    //============= private ========================================================================
    private fun pullArgs() {
        val args = requireArguments()

        val userParamArg: UserParam? = args.getParcelable(getString(R.string.args_nav_user_param))
        if (userParamArg == null) {
            navigate.back()
        } else {
            userParam = userParamArg
        }
    }

    private fun initTransitions() {
        layout.ivAvatar.transitionName = userParam.id.toString() + "imageView"
        val transitionSet = TransitionSet()
        transitionSet
            .addTransition(ChangeImageTransform())
            .addTransition(ChangeBounds())
            .addTransition(ChangeTransform())
        sharedElementEnterTransition = transitionSet
        sharedElementReturnTransition = transitionSet
        postponeEnterTransition()
    }

    private fun initDetailsUi() {
        setAvatar()
        layout.tvName.text = "..."
        layout.tvUrl.text = userParam.htmlUrl
    }

    private fun setAvatar() {
        clipCircle(layout.ivAvatar)
        val requestOptions = RequestOptions().dontTransform()
        Glide.with(this)
            .load(userParam.avatarUrl)
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
            .apply(requestOptions)
            .listener(object : RequestListener<Drawable?> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    startPostponedEnterTransition()
                    return false
                }
            })
            .into(layout.ivAvatar)
    }

    private fun initListeners() {
        layout.swipeRefresh.setOnRefreshListener { viewModel.fetchUserDetails(userParam.login) }
        layout.tvUrl.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            userParam.htmlUrl?.let {
                intent.data = Uri.parse(it)
                startActivity(intent)
            }
        }
    }

    //============= ViewModel ======================================================================
    override fun observeViewModelState() {
        super.observeViewModelState()

        with(viewModel) {
            observe(userDetailsLiveData, ::updateDetailsUi)
        }
    }

    override fun handleIsLoading(isLoading: Boolean) {
        layout.swipeRefresh.isRefreshing = isLoading
    }

    private fun updateDetailsUi(userDetails: UserDetails) {
        layout.tvName.text = userDetails.name
        layout.tvRepos.text = getString(R.string.txt_repos, userDetails.public_repos)
        layout.tvGists.text = getString(R.string.txt_gists, userDetails.public_gists)
        layout.tvFollowers.text = getString(R.string.txt_followers, userDetails.followers)
    }
}