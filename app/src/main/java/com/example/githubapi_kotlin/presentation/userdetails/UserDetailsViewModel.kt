package com.example.githubapi_kotlin.presentation.userdetails

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.domain.model.UserDetails
import com.example.domain.model.base.ResultBase
import com.example.domain.usecase.GetUserDetailsUseCase
import com.example.githubapi_kotlin.presentation.common.BaseViewModel
import com.example.githubapi_kotlin.presentation.common.Event
import com.example.githubapi_kotlin.presentation.userlist.UserListViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(private val getUserDetailsUseCase: GetUserDetailsUseCase) :
    BaseViewModel() {

    private val _userDetailsLiveData: MutableLiveData<UserDetails> = MutableLiveData()
    val userDetailsLiveData: LiveData<UserDetails> = _userDetailsLiveData

    //==============================================================================================
    fun fetchUserDetails(login: String?) {
        viewModelScope.launch {
            val loadingJob = launch {
                delay(loadingTimeout)
                isLoading(true)
            }

            getUserDetailsUseCase.execute(login)
                .onStart { Log.d(TAG, "fetchUserDetails: O.N.S.T.A.R.T. ${this.hashCode()}") }
                .onEach {
                    loadingJob.cancel()
                    isLoading(false)
                }
                .catch {
                    loadingJob.cancel()
                    isLoading(false)
                    Log.d(TAG, "fetchUserDetails: C.A.T.C.H.")
                }
                .onCompletion {
                    loadingJob.cancel()
                    isLoading(false)
                    Log.d(TAG, "fetchUserDetails: C.O.M.P.L.E.T.E. ${this.hashCode()}")
                }
                .collect { value ->
                    Log.d(TAG, "fetchUserDetails: collect() ${this.hashCode()}")
                    when (value) {
                        is ResultBase.Loading -> _isLoadingLiveData.postValue(value.status)
                        is ResultBase.Error -> _errorLiveData.postValue(Event(value.errorBody))
                        is ResultBase.Success -> _userDetailsLiveData.postValue(value.body)
                    }
                }
        }
    }

    companion object {
        val TAG: String = UserDetailsViewModel::class.java.simpleName
    }
}