package com.example.githubapi_kotlin.presentation.common

import androidx.appcompat.app.AppCompatActivity
import com.example.githubapi_kotlin.GitApp
import com.example.githubapi_kotlin.di.activity.BaseActivityComponent
import com.example.githubapi_kotlin.di.application.AppComponent

abstract class BaseActivity : AppCompatActivity() {

    private val appComponent: AppComponent get() = (application as GitApp).appLayerComponent

    val baseActivityComponent: BaseActivityComponent by lazy {
        appComponent.newBaseActivityComponentFactory().create(this)
    }
}