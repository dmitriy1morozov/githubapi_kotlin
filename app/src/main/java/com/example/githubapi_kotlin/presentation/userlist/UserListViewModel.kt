package com.example.githubapi_kotlin.presentation.userlist

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.domain.model.User
import com.example.domain.model.base.ResultBase
import com.example.domain.usecase.GetUsersUseCase
import com.example.domain.usecase.SearchUsersUseCase
import com.example.githubapi_kotlin.presentation.common.BaseViewModel
import com.example.githubapi_kotlin.presentation.common.Event
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

const val PER_PAGE = 20

class UserListViewModel @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase,
    private val searchUsersUseCase: SearchUsersUseCase
) : BaseViewModel() {

    private val userListSearch: MutableList<User> = mutableListOf()
    private var searchCriteria: String = ""
    private var page = 1

    private val userList: MutableList<User> = mutableListOf()

    private val _searchModeLiveData: MutableLiveData<Event<Boolean>> = MutableLiveData()
    val searchModeLiveData: LiveData<Event<Boolean>> = _searchModeLiveData

    private val _userListLiveData: MutableLiveData<List<User>> = MutableLiveData()
    val userListLiveData: LiveData<List<User>> = _userListLiveData

    init {
        refresh()
    }

    override fun onCleared() {
        super.onCleared()
        isLoading(false)
    }
    //==============================================================================================


    fun startSearchMode(search: String) {
        _searchModeLiveData.postValue(Event(true))
        userListSearch.clear()

        page = 1
        searchCriteria = search
        searchUsers(searchCriteria)
    }

    fun stopSearchMode() {
        _searchModeLiveData.postValue(Event(false))
        userListSearch.clear()

        _userListLiveData.postValue(userList)
        isLoading(false)
    }

    private fun searchUsers(criteria: String) {
        if (criteria.isEmpty()) {
            return
        }
        searchCriteria = criteria

        viewModelScope.launch {
            val loadingJob = launch {
                delay(loadingTimeout)
                isLoading(true)
            }

            val result = searchUsersUseCase.execute(searchCriteria, page, PER_PAGE)
            loadingJob.cancel()
            when (result) {
                is ResultBase.Loading -> isLoading(result.status)
                is ResultBase.Error -> handleError(result.errorBody)
                is ResultBase.Success -> {
                    isLoading(false)
                    if (searchModeLiveData.value?.peekContent() == false) {
                        return@launch
                    }
                    result.body.apply {
                        userListSearch.addAll(this)
                    }

                    _userListLiveData.postValue(userListSearch)
                }
            }
        }
    }


    fun refresh() {
        if (searchModeLiveData.value?.peekContent() == true) {
            startSearchMode(searchCriteria)
        } else {
            loadUsers(null)
        }
    }

    fun scrolledToBottom() {
        if (searchModeLiveData.value?.peekContent() == true) {
            page++
            searchUsers(searchCriteria)
        } else {
            val users: List<User>? = _userListLiveData.value
            val lastUserId: Int? = users?.lastOrNull()?.id
            loadUsers(lastUserId?.inc())
        }
    }

    private fun loadUsers(sinceUserId: Int?) {
        viewModelScope.launch {
            val loadingJob = launch {
                delay(loadingTimeout)
                isLoading(true)
            }

            getUsersUseCase.execute(sinceUserId)
                .onStart { Log.d(TAG, "loadUsers: O.N.S.T.A.R.T. ${this.hashCode()}") }
                .onEach {
                    loadingJob.cancel()
                    isLoading(false)
                }
                .catch {
                    loadingJob.cancel()
                    isLoading(false)
                    Log.d(TAG, "loadUsers: C.A.T.C.H.")
                }
                .onCompletion {
                    loadingJob.cancel()
                    isLoading(false)
                    Log.d(TAG, "loadUsers: C.O.M.P.L.E.T.E. ${this.hashCode()}")
                }
                .collect { value ->
                    Log.d(TAG, "loadUsers: COLLECT: ${this.hashCode()}")
                    when (value) {
                        is ResultBase.Loading -> isLoading(value.status)
                        is ResultBase.Error -> handleError(value.errorBody)
                        is ResultBase.Success -> { addUsers(value.body) }
                    }
                }
        }
    }

    private fun addUsers(users: List<User>) {
        for (user in users) {
            if (!userList.contains(user)) {
                userList.add(user)
            }
        }

        _userListLiveData.postValue(userList)
    }

    private fun handleError(error: String) {
        isLoading(false)
        _errorLiveData.postValue(Event(error))
    }

    companion object {
        private val TAG: String = UserListViewModel::class.java.simpleName
    }
}