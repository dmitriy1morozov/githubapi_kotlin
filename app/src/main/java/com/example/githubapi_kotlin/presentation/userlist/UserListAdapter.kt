package com.example.githubapi_kotlin.presentation.userlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.domain.model.User
import com.example.githubapi_kotlin.R
import com.example.githubapi_kotlin.databinding.ItemUserBinding
import com.example.githubapi_kotlin.presentation.userlist.UserListAdapter.UserVH
import com.example.githubapi_kotlin.utils.clipCircle

class UserListAdapter(private val userClickListener: UserClickListener) : Adapter<UserVH>() {

    interface UserClickListener {
        fun onUserClicked(user: User, transitionView: View)
    }

    private val userList: MutableList<User> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserVH {
        val inflater = LayoutInflater.from(parent.context)
        val holder = UserVH(ItemUserBinding.inflate(inflater))
        val lp = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        holder.layout.root.layoutParams = lp
        return holder
    }

    override fun onBindViewHolder(holder: UserVH, position: Int) {
        holder.bindUser(userList[position])
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    //==============================================================================================
    fun clearUserList() {
        val usersNew = emptyList<User>()
        val diffCallback = UserListDiffUtil(userList, usersNew)
        val diffResult = DiffUtil.calculateDiff(diffCallback, true)
        diffResult.dispatchUpdatesTo(this@UserListAdapter)
        userList.clear()
    }

    fun setUserList(usersNew: List<User>) {
        val diffCallback = UserListDiffUtil(userList, usersNew)
        val diffResult = DiffUtil.calculateDiff(diffCallback, true)
        diffResult.dispatchUpdatesTo(this@UserListAdapter)

        for (userNew in usersNew) {
            if (!userList.contains(userNew)) {
                userList.add(userNew)
            }
        }
    }

    fun isEmpty(): Boolean {
        return userList.isEmpty()
    }

    //==============================================================================================
    inner class UserVH(var layout: ItemUserBinding) : ViewHolder(layout.root) {

        fun bindUser(user: User) {
            layout.tvName.text = user.login
            clipCircle(layout.ivAvatar)
            val requestOptions = RequestOptions().dontTransform()
            Glide.with(itemView.context)
                    .load(user.avatar_url)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
                    .apply(requestOptions)
                    .into(layout.ivAvatar)
            layout.ivAvatar.transitionName = user.id.toString() + "imageView"
            layout.root.setOnClickListener { userClickListener.onUserClicked(user, layout.ivAvatar) }
        }
    }
}