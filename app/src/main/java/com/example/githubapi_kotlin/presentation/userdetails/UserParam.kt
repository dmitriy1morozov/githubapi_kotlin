package com.example.githubapi_kotlin.presentation.userdetails

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserParam(
    val id: Int,
    val login: String?,
    val avatarUrl: String?,
    val htmlUrl: String?
) : Parcelable