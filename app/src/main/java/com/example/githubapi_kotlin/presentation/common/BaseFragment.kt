package com.example.githubapi_kotlin.presentation.common

import android.content.ActivityNotFoundException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.githubapi_kotlin.R
import com.example.githubapi_kotlin.presentation.MainActivity
import com.example.githubapi_kotlin.utils.NavigationManager
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable

typealias Inflate<T> = (LayoutInflater) -> T

abstract class BaseFragment<out Binding : ViewBinding, out ViewModel : BaseViewModel>(
    private val inflate: Inflate<Binding>
) : Fragment() {

    private val baseActivityComponent get() = (requireActivity() as BaseActivity).baseActivityComponent
    protected val presentationComponent get() = baseActivityComponent.newPresentationComponent()

    val layout: Binding by lazy {
        inflate.invoke(layoutInflater)
    }


    protected val navigate: NavigationManager by lazy { NavigationManager(layout) }
    protected var disposable: CompositeDisposable = CompositeDisposable()

    private val sbNoInternet: Snackbar
        get() {
            val snackBar = Snackbar.make(layout.root, "", Snackbar.LENGTH_INDEFINITE)

            val snackView = layoutInflater.inflate(R.layout.snackbar_no_internet, null)
            (snackBar.view as ViewGroup).removeAllViews()
            (snackBar.view as ViewGroup).addView(snackView)
            return snackBar
        }

    //==============================================================================================
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layout.root
    }

    override fun onStart() {
        super.onStart()
        disposable = CompositeDisposable()
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    //==============================================================================================
    protected abstract fun handleIsLoading(isLoading: Boolean)

    protected open fun handleError(message: String?) {
        showToast(message)
    }

    protected open fun handleConnectionStateChanges(isConnected: Boolean) {
        if (isConnected) {
            sbNoInternet.dismiss()
        } else {
            sbNoInternet.show()
        }
    }

    protected open fun showToast(message: String?) {
        val mainActivity = activity as MainActivity?
        mainActivity?.showToast(message ?: return)
    }

    @Throws(Exception::class)
    private fun getParentActivity(): MainActivity {
        if (activity == null) {
            throw ActivityNotFoundException("No parent activity found")
        }
        return activity as MainActivity
    }
}