package com.example.githubapi_kotlin.presentation.userlist

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.navigation.fragment.FragmentNavigator.Extras.Builder
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.example.domain.model.User
import com.example.githubapi_kotlin.R
import com.example.githubapi_kotlin.databinding.FragmentUserListBinding
import com.example.githubapi_kotlin.presentation.common.BaseFragmentMvvm
import com.example.githubapi_kotlin.presentation.userdetails.UserParam
import com.example.githubapi_kotlin.presentation.userlist.UserListAdapter.UserClickListener
import com.example.githubapi_kotlin.utils.AnimationUtil
import com.example.githubapi_kotlin.utils.SpaceItemDecoration
import com.example.githubapi_kotlin.utils.afterMeasured
import com.example.githubapi_kotlin.utils.clipCircle
import com.jakewharton.rxbinding3.appcompat.queryTextChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


class UserListFragment :
    BaseFragmentMvvm<FragmentUserListBinding, UserListViewModel>(
        FragmentUserListBinding::inflate,
        UserListViewModel::class.java
    ) {

    private val userListAdapter: UserListAdapter by lazy {
        UserListAdapter(object : UserClickListener {
            override fun onUserClicked(user: User, transitionView: View) {
                val args = Bundle()
                val userParam = UserParam(user.id, user.login, user.avatar_url, user.html_url)
                args.putParcelable(getString(R.string.args_nav_user_param), userParam)
                val extras = Builder()
                    .addSharedElement(transitionView, transitionView.transitionName)
                    .build()
                navigate.to(R.id.nav_userList_userDetails, args, extras)
            }
        })
    }

    //============= Lifecycle ======================================================================
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presentationComponent.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        initUi()
        initListeners()
    }

    //============= private ========================================================================
    private fun initRecycler() {
        if (layout.rvUsers.layoutManager == null || layout.rvUsers.adapter == null) {
            val layoutManager = LinearLayoutManager(context)
            layout.rvUsers.layoutManager = layoutManager
            layout.rvUsers.adapter = userListAdapter
            layout.rvUsers.addItemDecoration(SpaceItemDecoration(8, 8, 8))
            layout.rvUsers.addOnScrollListener(object : OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val lastVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition()
                    if (lastVisiblePosition == userListAdapter.itemCount - 1) {
                        viewModel.scrolledToBottom()
                    }
                }
            })
        }
    }

    private fun initUi() {
        if (viewModel.searchModeLiveData.value?.peekContent() == true) {
            layout.btnSearch.alpha = 0f
            layout.btnSearch.visibility = View.INVISIBLE

            layout.svSearch.alpha = 1f
            layout.svSearch.visibility = View.VISIBLE
        } else {
            layout.btnSearch.alpha = 1f
            layout.btnSearch.translationY = 0f
            layout.btnSearch.visibility = View.VISIBLE

            layout.svSearch.alpha = 0f
            layout.svSearch.visibility = View.INVISIBLE
        }
    }

    private fun initListeners() {
        clipCircle(layout.svSearch)
        layout.swipeRefresh.setOnRefreshListener { viewModel.refresh() }

        layout.afterMeasured(layout) {
            val translateY: Float = (layout.svSearch.top - layout.btnSearch.top).toFloat()
            val fabForward = getFabForward(translateY)
            val fabBackward = getFabBackward(translateY)

            layout.btnSearch.setOnClickListener {
                fabForward.start()
            }

            layout.svSearch.setOnCloseListener {
                AnimationUtil.getFadeOut(layout.svSearch).start()
                fabBackward.start()
                layout.appBar.setExpanded(false, true)
                false
            }
        }

        disposable.add(
            layout.svSearch.queryTextChanges()
                .skipInitialValue()
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter { it.length > 2 }
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { search: CharSequence ->
                        Log.d("queryTextChanges", "initListeners: search = $search")
                        viewModel.startSearchMode(search.toString())
                    },
                    { error: Throwable ->
                        showToast("Something went wrong: $error")
                    }
                )
        )
    }

    private fun getFabForward(translateY: Float): AnimatorSet {
        val fabForward = getFabForwardAnimator(translateY)

        fabForward.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                layout.btnSearch.visibility = View.INVISIBLE
                layout.svSearch.visibility = View.VISIBLE

                AnimationUtil.getFadeIn(layout.svSearch).start()
                layout.svSearch.isIconified = false

                layout.appBar.setExpanded(true, true)
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationStart(animation: Animator) {
                layout.btnSearch.visibility = View.VISIBLE
                layout.svSearch.visibility = View.VISIBLE
            }
        })

        return fabForward
    }

    private fun getFabBackward(translateY: Float): AnimatorSet {
        val fabBackward = getFabBackwardAnimator(translateY)

        fabBackward.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                layout.btnSearch.visibility = View.VISIBLE
                layout.svSearch.visibility = View.INVISIBLE

                viewModel.stopSearchMode()
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationStart(animation: Animator) {
                layout.btnSearch.visibility = View.VISIBLE
                layout.svSearch.visibility = View.VISIBLE
            }
        })

        return fabBackward
    }

    private fun getFabForwardAnimator(translateY: Float): AnimatorSet {
        val fadeOutFab = AnimationUtil.getFadeOut(layout.btnSearch)

        val translation =
            ObjectAnimator.ofFloat(layout.btnSearch, View.TRANSLATION_Y, 0f, translateY)
        translation.duration = 300
        translation.interpolator = DecelerateInterpolator()

        val animatorSet = AnimatorSet()
        animatorSet.playSequentially(translation, fadeOutFab)
        return animatorSet
    }

    private fun getFabBackwardAnimator(translateY: Float): AnimatorSet {
        val fadeInFab = AnimationUtil.getFadeIn(layout.btnSearch)

        val translation =
            ObjectAnimator.ofFloat(layout.btnSearch, View.TRANSLATION_Y, translateY, 0f)
        translation.duration = 300
        translation.interpolator = DecelerateInterpolator()

        val animatorSet = AnimatorSet()
        animatorSet.playSequentially(fadeInFab, translation)
        return animatorSet
    }

    //============= ViewModel ======================================================================
    override fun observeViewModelState() {
        super.observeViewModelState()

        with(viewModel) {
            observe(searchModeLiveData) { event ->
                event.popContent()?.let {
                    userListAdapter.clearUserList()
                }
            }

            observe(userListLiveData) { users: List<User> ->
                userListAdapter.setUserList(users)
                checkPlaceholder()
            }
        }
    }

    override fun handleIsLoading(isLoading: Boolean) {
        layout.swipeRefresh.isRefreshing = isLoading
    }

    override fun handleConnectionStateChanges(isConnected: Boolean) {
        super.handleConnectionStateChanges(isConnected)
        checkPlaceholder()
    }

    override fun handleError(message: String?) {
        super.handleError(message)
        checkPlaceholder()
    }

    private fun checkPlaceholder() {
        if (userListAdapter.isEmpty()) {
            layout.ivPlaceholder.visibility = View.VISIBLE
        } else {
            layout.ivPlaceholder.visibility = View.GONE
        }
    }
}