package com.example.githubapi_kotlin.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.githubapi_kotlin.R
import com.example.githubapi_kotlin.presentation.common.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onSupportNavigateUp(): Boolean {
        return Navigation.findNavController(this, R.id.navHostFragment).navigateUp()
    }

    fun showToast(message: String?) {
        if (!this.isFinishing) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }
}