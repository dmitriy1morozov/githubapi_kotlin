package com.example.data

import android.util.Log
import com.example.data.datasource.local.UserDataSourceLocal
import com.example.data.datasource.remote.UserDataSourceRemote
import com.example.data.di.async.IoDispatcher
import com.example.data.model.ResultData
import com.example.data.model.toDomain
import com.example.domain.UserRepository
import com.example.domain.model.User
import com.example.domain.model.UserDetails
import com.example.domain.model.base.ResultBase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import javax.inject.Inject

internal class UserRepositoryImpl @Inject constructor(
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
    private val userDataSourceLocal: UserDataSourceLocal,
    private val userDataSourceRemote: UserDataSourceRemote
) : UserRepository {

    // This is local-first caching strategy
    override fun getUserDetails(login: String): Flow<ResultBase<UserDetails>> =
        channelFlow<ResultData<UserDetails>> {
            launch { getUserDetailsRemote(login, this@channelFlow) }
            launch { getUserDetailsLocal(login, this@channelFlow) }
        }
            .onEach { Log.d(TAG, "getUserDetails: ON_EACH $it") }
            .distinctUntilChanged(::distinctChanges)
            .onEach { Log.d(TAG, "getUserDetails: ON_EACH_distinctUntilChanged $it") }
            .map { resultData ->
                resultData.toDomain { it }
            }
            .onCompletion { Log.d(TAG, "getUserDetails: COMPLETE") }
            .flowOn(ioDispatcher)

    private suspend fun getUserDetailsRemote(
        login: String,
        channel: ProducerScope<ResultData<UserDetails>>
    ) =
        withTimeout(TIMEOUT) {
            delay(1000)
            val remote: ResultData<UserDetails> = userDataSourceRemote.getUserDetails(login)
            if (remote is ResultData.Success) {
                userDataSourceLocal.upsertUserDetails(remote.body)
            }
            channel.send(remote)
            cancel()
        }

    private suspend fun getUserDetailsLocal(
        login: String,
        channel: ProducerScope<ResultData<UserDetails>>
    ) =
        withTimeout(TIMEOUT) {
            val local: UserDetails? = userDataSourceLocal.getUserDetails(login)
            local?.let {
                channel.send(ResultData.successLocal(local))
            }
            cancel()
        }

    private fun <T : Any> distinctChanges(old: ResultData<T>, new: ResultData<T>) =
        if (old is ResultData.Success && new is ResultData.Success) {
            old.body == new.body
        } else {
            false
        }

    // This is local-first caching strategy
    override fun getUsers(sinceId: Int): Flow<ResultBase<List<User>>> =
        channelFlow<ResultData<List<User>>> {
            launch { getUsersRemote(sinceId, this@channelFlow) }
            launch { getUsersLocal(sinceId, this@channelFlow) }
        }
            .onEach { Log.d(TAG, "getUsers: ON_EACH $it") }
            .distinctUntilChanged(::distinctChanges)
            .onEach { Log.d(TAG, "getUsers: ON_EACH_distinctUntilChanged $it") }
            .map { resultData ->
                resultData.toDomain { it }
            }
            .onCompletion { Log.d(TAG, "getUsers: COMPLETE") }
            .flowOn(ioDispatcher)

    private suspend fun getUsersRemote(
        sinceId: Int,
        channel: ProducerScope<ResultData<List<User>>>
    ) =
        withTimeout(TIMEOUT) {
            delay(1000)
            val remote: ResultData<List<User>> = userDataSourceRemote.getUsers(sinceId.toString())
            if (remote is ResultData.Success) {
                userDataSourceLocal.upsertUsers(remote.body)
            }
            channel.send(remote)
            cancel()
        }

    private suspend fun getUsersLocal(
        sinceId: Int,
        channel: ProducerScope<ResultData<List<User>>>
    ) =
        withTimeout(TIMEOUT) {
            val local: List<User> = userDataSourceLocal.getUsers(sinceId)
            if (local.isNotEmpty()) {
                channel.send(ResultData.successLocal(local))
            }
            cancel()
        }

    override suspend fun searchUsers(
        criteria: String?,
        page: Int?,
        perPage: Int?
    ): ResultBase<List<User>> =
        withContext(ioDispatcher) {
            val search: String? = if (criteria?.isEmpty() != false) null else criteria

            val remoteResult = userDataSourceRemote.searchUsers(search, page, perPage)
            when (remoteResult) {
                is ResultData.Success -> ResultBase.Success(remoteResult.body)
                is ResultData.Error -> ResultBase.Error(remoteResult.errorBody)
            }
        }

    companion object {
        val TAG: String = UserRepositoryImpl::class.java.simpleName
        private const val TIMEOUT = 10000L
    }
}
