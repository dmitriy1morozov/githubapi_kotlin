package com.example.data

import android.content.Context
import com.facebook.flipper.android.AndroidFlipperClient
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.core.FlipperClient
import com.facebook.flipper.core.FlipperPlugin
import com.facebook.soloader.SoLoader


class GitApp(private val appContext: Context, private val flipperPlugins: Set<FlipperPlugin>) {

    fun onCreate() {
        SoLoader.init(appContext, false)
        if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(appContext)) {
            val client: FlipperClient = AndroidFlipperClient.getInstance(appContext)
            flipperPlugins.forEach(client::addPlugin)
            client.start()
        }
    }
}