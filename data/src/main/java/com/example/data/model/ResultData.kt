package com.example.data.model

import com.example.domain.model.base.ResultBase


internal fun <DataModel : Any, DomainModel : Any> ResultData<DataModel>.toDomain(
    mapDataToDomain: (dataModel: DataModel) -> DomainModel
): ResultBase<DomainModel> {
    return when (this) {
        is ResultData.Success -> ResultBase.Success(mapDataToDomain(body))
        is ResultData.Error -> ResultBase.Error(errorBody)
    }
}

internal sealed class ResultData<out Success : Any>(val source: Source) {

    /**
     * Success response with body
     */
    data class Success<Success : Any>(val src: Source, val body: Success) : ResultData<Success>(src)

    /**
     * Failure response with errorBody: String and optional code:Int?
     */
    data class Error(val src: Source, val errorBody: String, val code: Int = -1) :
        ResultData<Nothing>(src)

    companion object {
        fun <Success : Any> successLocal(body: Success) = Success(Source.LOCAL, body)
        fun <Success : Any> successRemote(body: Success) = Success(Source.REMOTE, body)
        fun errorLocal(errorBody: String, code: Int = -1) = Error(Source.LOCAL, errorBody, code)
        fun errorRemote(errorBody: String, code: Int = -1) = Error(Source.REMOTE, errorBody, code)
    }
}

enum class Source {
    LOCAL, REMOTE
}