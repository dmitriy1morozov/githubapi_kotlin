package com.example.data.model.network.response

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class NetworkError(val message: String)