package com.example.data.model.mapper

import com.example.data.model.database.UserDb
import com.example.data.model.database.UserDetailsDb
import com.example.data.model.network.UserDetailsNetwork
import com.example.data.model.network.UserNetwork
import com.example.domain.model.User
import com.example.domain.model.UserDetails

internal fun User.toDb(): UserDb = UserDb(id, login, avatar_url, html_url)

internal fun UserDetails.toDb(): UserDetailsDb =
    UserDetailsDb(login, name, public_repos, public_gists, followers, id)


internal fun UserDb.toDomain(): User =
    User(
        avatarUrl,
        htmlUrl,
        id,
        login
    )

internal fun UserDetailsDb.toDomain(): UserDetails =
    UserDetails(
        login,
        followers,
        id,
        name,
        publicGists,
        publicRepos
    )

internal fun UserNetwork.toDomain(): User =
    User(
        avatar_url,
        html_url,
        id,
        login
    )


internal fun UserDetailsNetwork.toDomain(): UserDetails =
    UserDetails(
        login,
        followers,
        id,
        name,
        public_gists,
        public_repos
    )