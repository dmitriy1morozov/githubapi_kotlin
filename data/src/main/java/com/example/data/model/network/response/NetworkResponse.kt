package com.example.data.model.network.response

internal sealed class NetworkResponse<out Success : Any, out Error : Any> {
    /**
     * Success response with body
     */
    data class Success<Success : Any>(val body: Success) : NetworkResponse<Success, Nothing>()

    /**
     * Failure response with body
     */
    data class Error<Error : Any>(val error: Error, val code: Int) :
        NetworkResponse<Nothing, Error>()

    /**
     * For example, json parsing error
     */
    data class Exception(val error: Throwable?) : NetworkResponse<Nothing, Nothing>()
}
