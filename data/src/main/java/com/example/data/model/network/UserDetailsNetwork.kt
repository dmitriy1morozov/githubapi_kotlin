package com.example.data.model.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class UserDetailsNetwork(
        @Transient val avatar_url: String = "",
        @Transient val bio: String = "",
        @Transient val blog: String = "",
        @Transient val company: String = "",
        @Transient val created_at: String = "",
        @Transient val email: String = "",
        @Transient val events_url: String = "",
        val followers: Int,
        @Transient val followers_url: String = "",
        @Transient val following: Int = 0,
        @Transient val following_url: String = "",
        @Transient val gists_url: String = "",
        @Transient val gravatar_id: String = "",
        @Transient val hireable: Any = "",
        @Transient val html_url: String = "",
        @Transient val id: Int = 0,
        @Transient val location: String = "",
        val login: String,
        val name: String?,
        @Transient val node_id: String = "",
        @Transient val organizations_url: String = "",
        val public_gists: Int,
        val public_repos: Int,
        @Transient val received_events_url: String = "",
        @Transient val repos_url: String = "",
        @Transient val site_admin: Boolean = true,
        @Transient val starred_url: String = "",
        @Transient val subscriptions_url: String = "",
        @Transient val type: String = "",
        @Transient val updated_at: String = "",
        @Transient val url: String = ""
)