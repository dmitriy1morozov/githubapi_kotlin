package com.example.data.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
internal data class UserDb(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "login") val login: String?,
    @ColumnInfo(name = "avatarUrl") val avatarUrl: String?,
    @ColumnInfo(name = "htmlUrl") val htmlUrl: String?
)