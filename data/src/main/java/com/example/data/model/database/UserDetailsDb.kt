package com.example.data.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["name"], unique = true)])
internal data class UserDetailsDb(
    @PrimaryKey @ColumnInfo(name = "login") val login: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "public_repos") val publicRepos: Int?,
    @ColumnInfo(name = "public_gists") val publicGists: Int?,
    @ColumnInfo(name = "followers") val followers: Int?,
    @ColumnInfo(name = "id") val id: Int?
) {
//    @PrimaryKey(autoGenerate = true)
//    var id: Int = 0
}