package com.example.data.model.network

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
internal data class UserNetwork(
        val avatar_url: String?,
        @Transient val events_url: String? = null,
        @Transient val followers_url: String? = null,
        @Transient val following_url: String? = null,
        @Transient val gists_url: String? = null,
        @Transient val gravatar_id: String? = null,
        val html_url: String?,
        val id: Int,
        val login: String?,
        @Transient val node_id: String? = null,
        @Transient val organizations_url: String? = null,
        @Transient val received_events_url: String? = null,
        @Transient val repos_url: String? = null,
        @Transient val score: Float? = 0f,
        @Transient val site_admin: Boolean = true,
        @Transient val starred_url: String? = null,
        @Transient val subscriptions_url: String? = null,
        @Transient val type: String? = null,
        @Transient val url: String? = null
) : Parcelable