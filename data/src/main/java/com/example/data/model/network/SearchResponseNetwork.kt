package com.example.data.model.network

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class SearchResponseNetwork<Result>(
        val incomplete_results: Boolean,
        val total_count: Int,
        val items: List<Result>
)