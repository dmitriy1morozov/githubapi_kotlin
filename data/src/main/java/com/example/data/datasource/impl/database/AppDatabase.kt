package com.example.data.datasource.impl.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.model.database.UserDb
import com.example.data.model.database.UserDetailsDb

@Database(entities = [UserDb::class, UserDetailsDb::class], version = 1)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}