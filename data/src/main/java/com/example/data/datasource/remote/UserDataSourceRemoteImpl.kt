package com.example.data.datasource.remote

import com.example.data.datasource.impl.network.UsersEndpoint
import com.example.data.model.ResultData
import com.example.data.model.mapper.toDomain
import com.example.data.model.network.SearchResponseNetwork
import com.example.data.model.network.UserDetailsNetwork
import com.example.data.model.network.UserNetwork
import com.example.data.model.network.response.NetworkError
import com.example.data.model.network.response.NetworkResponse
import com.example.domain.model.User
import com.example.domain.model.UserDetails
import javax.inject.Inject

internal class UserDataSourceRemoteImpl @Inject constructor(private val usersEndpoint: UsersEndpoint) :
    UserDataSourceRemote {

    override suspend fun getUserDetails(userName: String?): ResultData<UserDetails> {
        val networkResult: NetworkResponse<UserDetailsNetwork, NetworkError> =
            try {
                usersEndpoint.getUserDetails(userName)
            } catch (e: RuntimeException) {
                NetworkResponse.Exception(e)
            }

        return when (networkResult) {
            is NetworkResponse.Success -> ResultData.successRemote(networkResult.body.toDomain())
            is NetworkResponse.Error -> ResultData.errorRemote(networkResult.error.message)
            is NetworkResponse.Exception -> ResultData.errorRemote(
                networkResult.error?.message ?: "Unknown Exception"
            )
        }
    }

    override suspend fun getUsers(sinceId: String?): ResultData<List<User>> {
        val networkResult: NetworkResponse<List<UserNetwork>, NetworkError> =
            try {
                usersEndpoint.getUsers(sinceId)
            } catch (e: RuntimeException) {
                NetworkResponse.Exception(e)
            }

        return when (networkResult) {
            is NetworkResponse.Success -> {
                ResultData.successRemote(networkResult.body.map { userNetwork -> userNetwork.toDomain() })
            }
            is NetworkResponse.Error -> ResultData.errorRemote(networkResult.error.message)
            is NetworkResponse.Exception -> ResultData.errorRemote(
                networkResult.error?.message ?: "Unknown Exception"
            )
        }
    }

    override suspend fun searchUsers(
        criteria: String?,
        page: Int?,
        perPage: Int?
    ): ResultData<List<User>> {
        val search: String? = if (criteria?.isEmpty() != false) null else criteria

        val networkResult: NetworkResponse<SearchResponseNetwork<UserNetwork>, NetworkError> =
            try {
                usersEndpoint.searchUsers(search, page, perPage)
            } catch (e: RuntimeException) {
                NetworkResponse.Exception(e)
            }

        return when (networkResult) {
            is NetworkResponse.Success -> {
                ResultData.successRemote(networkResult.body.items.map { userNetwork -> userNetwork.toDomain() })
            }
            is NetworkResponse.Error -> ResultData.errorRemote(networkResult.error.message)
            is NetworkResponse.Exception -> ResultData.errorRemote(
                networkResult.error?.message ?: "Unknown Exception"
            )
        }
    }
}