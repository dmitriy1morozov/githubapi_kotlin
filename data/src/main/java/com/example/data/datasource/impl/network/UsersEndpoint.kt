package com.example.data.datasource.impl.network

import com.example.data.model.network.response.NetworkError
import com.example.data.model.network.response.NetworkResponse
import com.example.data.model.network.SearchResponseNetwork
import com.example.data.model.network.UserDetailsNetwork
import com.example.data.model.network.UserNetwork
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface UsersEndpoint {

    @GET("users")
    suspend fun getUsers(@Query("since") sinceId: String?): NetworkResponse<List<UserNetwork>, NetworkError>

    @GET("search/users")
    suspend fun searchUsers(
        @Query("q") criteria: String?,
        @Query("page") page: Int?,
        @Query("per_page") perPage: Int?
    ): NetworkResponse<SearchResponseNetwork<UserNetwork>, NetworkError>

    @GET("users/{user_name}")
    suspend fun getUserDetails(@Path("user_name") userName: String?): NetworkResponse<UserDetailsNetwork, NetworkError>
}