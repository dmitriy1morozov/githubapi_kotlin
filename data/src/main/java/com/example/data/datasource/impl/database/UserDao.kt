package com.example.data.datasource.impl.database

import androidx.room.*
import com.example.data.model.database.UserDb
import com.example.data.model.database.UserDetailsDb
import kotlinx.coroutines.flow.Flow

@Dao
internal interface UserDao {

    @Query("SELECT * FROM userDb")
    suspend fun getAllUsers(): List<UserDb>

    @Query("SELECT * FROM userDb WHERE id >= :startId ORDER BY id ASC LIMIT :limit")
    suspend fun getUsers(startId: Int, limit: Int): List<UserDb?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertUsers(vararg users: UserDb)

    @Delete
    suspend fun deleteUser(user: UserDb)


    @Query("SELECT * FROM UserDetailsDb WHERE login LIKE :login LIMIT 1")
    suspend fun findUserDetails(login: String): UserDetailsDb?


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertUserDetails(userDetails: UserDetailsDb)

    @Delete
    suspend fun deleteUserDetails(userDetails: UserDetailsDb)
}