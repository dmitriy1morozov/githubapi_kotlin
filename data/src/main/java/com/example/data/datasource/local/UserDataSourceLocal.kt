package com.example.data.datasource.local

import com.example.data.model.ResultData
import com.example.domain.model.User
import com.example.domain.model.UserDetails
import kotlinx.coroutines.flow.Flow

internal interface UserDataSourceLocal {

    suspend fun upsertUsers(users: List<User>)

    suspend fun upsertUserDetails(userDetails: UserDetails)

    suspend fun getUsers(sinceId: Int, limit: Int = PER_PAGE): List<User>

    suspend fun getUserDetails(userName: String): UserDetails?

    companion object {
        const val PER_PAGE = 30
    }
}