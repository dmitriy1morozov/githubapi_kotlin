package com.example.data.datasource.remote

import com.example.data.model.ResultData
import com.example.domain.model.User
import com.example.domain.model.UserDetails

internal interface UserDataSourceRemote {

    suspend fun getUserDetails(userName: String?): ResultData<UserDetails>

    suspend fun getUsers(sinceId: String?): ResultData<List<User>>

    suspend fun searchUsers(
        criteria: String?,
        page: Int?,
        perPage: Int?
    ): ResultData<List<User>>
}