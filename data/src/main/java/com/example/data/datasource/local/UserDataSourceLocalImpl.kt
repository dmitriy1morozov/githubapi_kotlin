package com.example.data.datasource.local

import android.util.Log
import com.example.data.datasource.DB_CODE_NO_RECORDS
import com.example.data.datasource.DB_MESSAGE_NO_RECORDS
import com.example.data.datasource.impl.database.UserDao
import com.example.data.model.ResultData
import com.example.data.model.mapper.toDb
import com.example.data.model.mapper.toDomain
import com.example.domain.model.User
import com.example.domain.model.UserDetails
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

internal class UserDataSourceLocalImpl @Inject constructor(private val userDao: UserDao) :
    UserDataSourceLocal {

    override suspend fun upsertUsers(users: List<User>) {
        Log.d(TAG, "upsertUsers: ")
        val usersDb = users.map { it.toDb() }
        userDao.upsertUsers(*usersDb.toTypedArray())
    }

    override suspend fun upsertUserDetails(userDetails: UserDetails) {
        Log.d(TAG, "upsertUserDetails: ")
        val userDetailsDb = userDetails.toDb()
        userDao.upsertUserDetails(userDetailsDb)
    }

    override suspend fun getUsers(sinceId: Int, limit: Int): List<User> {
        return userDao.getUsers(sinceId, limit)
            .filterNotNull()
            .map { it.toDomain() }
    }

    override suspend fun getUserDetails(userName: String): UserDetails? {
        return userDao.findUserDetails(userName)?.toDomain()
    }

    companion object {
        private val TAG = UserDataSourceLocalImpl::class.java.simpleName
    }
}