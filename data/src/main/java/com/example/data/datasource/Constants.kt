package com.example.data.datasource

const val DB_CODE_NO_RECORDS = -101
const val DB_MESSAGE_NO_RECORDS = "No records found in DB"