package com.example.data.di.network

import javax.inject.Qualifier

@Qualifier
internal annotation class BasicAuth
