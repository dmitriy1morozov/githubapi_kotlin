package com.example.data.di.async

import javax.inject.Qualifier

@Qualifier
internal annotation class IoDispatcher
