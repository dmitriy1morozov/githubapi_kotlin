package com.example.data.di.appcontext

import android.app.Application
import android.content.Context
import com.example.data.di.DataScope
import dagger.Module
import dagger.Provides

@Module
class AppContextModule(private val application: Application) {

    @Provides
    @DataScope
    fun provideAppContext(): Context = application.applicationContext
}