package com.example.data.di.network

import com.example.data.BuildConfig
import com.example.data.datasource.impl.network.NetworkResponseAdapterFactory
import com.example.data.datasource.impl.network.RetrofitConfig
import com.example.data.di.DataScope
import com.facebook.flipper.plugins.network.FlipperOkhttpInterceptor
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@Module
internal class NetworkModule {

    @DataScope
    @Provides
    fun provideRetrofit(
        converterFactory: Converter.Factory,
        callAdapterFactory: CallAdapter.Factory,
        okHttpClient: OkHttpClient
    ): Retrofit =
        Retrofit.Builder()
            .baseUrl(RetrofitConfig.REST_BASE_URL)
            .addCallAdapterFactory(callAdapterFactory)
            .addConverterFactory(converterFactory)
            .client(okHttpClient)
            .build()


    @DataScope
    @Provides
    fun provideMoshiConverterFactory(moshi: Moshi): Converter.Factory =
        MoshiConverterFactory.create(moshi)

    @DataScope
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder().build()

    @DataScope
    @Provides
    fun provideCoroutineCallAdapterFactory(): CallAdapter.Factory = NetworkResponseAdapterFactory()


    @DataScope
    @Provides
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        networkFlipperPlugin: NetworkFlipperPlugin,
        @BasicAuth interceptor: Interceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addNetworkInterceptor(FlipperOkhttpInterceptor(networkFlipperPlugin))
            .addInterceptor(interceptor)
            .addInterceptor(loggingInterceptor)
            .build()


    @DataScope
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return loggingInterceptor
    }

    @DataScope
    @Provides
    @BasicAuth
    fun provideInterceptor(): Interceptor =
        Interceptor { chain: Interceptor.Chain ->
            val builder: Request.Builder = chain.request().newBuilder()
            builder.header(
                "Authorization",
                Credentials.basic(RetrofitConfig.REST_BASIC_USER, RetrofitConfig.REST_BASIC_TOKEN)
            ).build()
            chain.proceed(builder.build())
        }
}