package com.example.data.di.network.endpoints

import com.example.data.datasource.impl.network.UsersEndpoint
import com.example.data.di.DataScope
import com.example.data.di.network.NetworkModule
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module(includes = [NetworkModule::class])
internal class UserEndpointModule {

    @DataScope
    @Provides
    fun provideUsersEndpoint(retrofit: Retrofit):UsersEndpoint = retrofit.create(UsersEndpoint::class.java)
}