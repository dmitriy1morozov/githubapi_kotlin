package com.example.data.di.debug

import com.example.data.di.DataScope
import com.facebook.flipper.core.FlipperPlugin
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

@Module(includes = [FlipperModuleImpl::class])
interface FlipperModule {

    @DataScope
    @IntoSet
    @Binds
    fun provideNetworkPlugin(plugin: NetworkFlipperPlugin): FlipperPlugin

    @DataScope
    @IntoSet
    @Binds
    fun provideInspectorPlugin(plugin: InspectorFlipperPlugin): FlipperPlugin

    @DataScope
    @IntoSet
    @Binds
    fun provideDatabasePlugin(plugin: DatabasesFlipperPlugin): FlipperPlugin
}