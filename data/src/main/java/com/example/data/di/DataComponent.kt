package com.example.data.di

import com.example.data.GitApp
import com.example.data.di.appcontext.AppContextModule
import com.example.data.di.async.DispatchersModule
import com.example.data.di.database.RoomDbModule
import com.example.data.di.datasource.DataSourceModule
import com.example.data.di.debug.FlipperModule
import com.example.data.di.network.endpoints.UserEndpointModule
import com.example.domain.UserRepository
import dagger.Component

@DataScope
@Component(
    modules = [
        DataModule::class,
        AppContextModule::class,
        DispatchersModule::class,
        RoomDbModule::class,
        UserEndpointModule::class,
        DataSourceModule::class,
        FlipperModule::class,
        GitAppModule::class
    ]
)
interface DataComponent {

    @Component.Factory
    interface Factory {
        fun create(appContextModule: AppContextModule): DataComponent
    }

    fun getUserRepository(): UserRepository

    fun getGitApp(): GitApp
}