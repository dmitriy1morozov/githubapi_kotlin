package com.example.data.di.datasource

import com.example.data.datasource.impl.database.UserDao
import com.example.data.datasource.impl.network.UsersEndpoint
import com.example.data.datasource.local.UserDataSourceLocalImpl
import com.example.data.datasource.remote.UserDataSourceRemoteImpl
import com.example.data.di.DataScope
import dagger.Module
import dagger.Provides

@Module
internal class DataSourceImplModule {

    @DataScope
    @Provides
    fun provideUserDataSourceLocalImpl(userDao: UserDao): UserDataSourceLocalImpl =
        UserDataSourceLocalImpl(userDao)

    @DataScope
    @Provides
    fun provideUserDataSourceRemoteImpl(usersEndpoint: UsersEndpoint): UserDataSourceRemoteImpl =
        UserDataSourceRemoteImpl(usersEndpoint)
}