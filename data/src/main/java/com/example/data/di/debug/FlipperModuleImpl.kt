package com.example.data.di.debug

import android.content.Context
import com.example.data.di.DataScope
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin
import com.facebook.flipper.plugins.inspector.DescriptorMapping
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin
import dagger.Module
import dagger.Provides

@Module
class FlipperModuleImpl {

    @DataScope
    @Provides
    fun provideNetworkPlugin(): NetworkFlipperPlugin = NetworkFlipperPlugin()

    @DataScope
    @Provides
    fun provideInspectorPlugin(context: Context): InspectorFlipperPlugin =
        InspectorFlipperPlugin(context, DescriptorMapping.withDefaults())

    @DataScope
    @Provides
    fun provideDatabasePlugin(context: Context): DatabasesFlipperPlugin =
        DatabasesFlipperPlugin(context)
}