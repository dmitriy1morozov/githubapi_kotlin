package com.example.data.di

import android.content.Context
import com.example.data.GitApp
import com.facebook.flipper.core.FlipperPlugin
import dagger.Module
import dagger.Provides

@Module
class GitAppModule {

    @DataScope
    @Provides
    fun provideGitApp(appContext: Context, flipperPlugins: Set<@JvmSuppressWildcards FlipperPlugin>): GitApp =
        GitApp(appContext, flipperPlugins)
}