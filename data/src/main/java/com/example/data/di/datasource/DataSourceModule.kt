package com.example.data.di.datasource

import com.example.data.datasource.local.UserDataSourceLocal
import com.example.data.datasource.local.UserDataSourceLocalImpl
import com.example.data.datasource.remote.UserDataSourceRemote
import com.example.data.datasource.remote.UserDataSourceRemoteImpl
import com.example.data.di.DataScope
import dagger.Binds
import dagger.Module

@Module(includes = [DataSourceImplModule::class])
internal interface DataSourceModule {

    @DataScope
    @Binds
    fun provideUserDataSourceLocal(dataSource: UserDataSourceLocalImpl): UserDataSourceLocal

    @DataScope
    @Binds
    fun provideUserDataSourceRemote(dataSource: UserDataSourceRemoteImpl): UserDataSourceRemote
}