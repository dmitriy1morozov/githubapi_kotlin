package com.example.data.di

import javax.inject.Scope

@Scope
internal annotation class DataScope
