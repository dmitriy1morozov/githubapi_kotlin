package com.example.data.di.database

import android.content.Context
import androidx.room.Room
import com.example.data.datasource.impl.database.AppDatabase
import com.example.data.datasource.impl.database.UserDao
import com.example.data.di.DataScope
import dagger.Module
import dagger.Provides

@Module
internal class RoomDbModule {

    @DataScope
    @Provides
    fun provideRoomDb(appContext: Context): AppDatabase =
        Room.databaseBuilder(appContext, AppDatabase::class.java, "git-users").build()

    @DataScope
    @Provides
    fun provideUserDao(db: AppDatabase): UserDao = db.userDao()
}