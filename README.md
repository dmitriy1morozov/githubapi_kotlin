Primitive application with couple screens

I created this pet-project with very limited features and code in order to concentrate on the architectural approaches, latest technologies and DependencyInjection. All the business-logic is limited intentionally.

Technologies used: 

- Dependency Injection architectural pattern with Dagger2
- Clean architecture with separate data/domain/app layers. Check the CleanArchitecture.png for diagram
- Each layer is encapsulated into its appropriate module
- Google's MVVM is used for presentation layer
- LiveData is used for Presentation layer only
- Coroutines and Flows are used for async calls and for reactive streams of data to wire-up the layers with data transportation
- Primitive localDb-first caching strategy is used
- Room is used for DB

![](/images/CleanArchitecture.png)


![](/images/demo.gif)
